import { defineConfig } from 'astro/config';

import cloudflare from "@astrojs/cloudflare";

// https://astro.build/config
export default defineConfig({
  output: "server",
  adapter: cloudflare({
    mode: 'directory',
    // functionPerRoute: true

    routes: {
      strategy: 'auto',
      include: ['/helloworld'], // handled by custom function: functions/users/[id].js
      exclude: ['/users/faq'], // handled by static page: pages/users/faq.astro
    },
  })
});