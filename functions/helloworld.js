// export function onRequest(context) {
  // if (context.env.ENVIRONMENT === 'production') {
	// 	return new Response('This is a production environment!');
	// } else {
	// 	return new Response('This is a live environment');
	// }

  // export async function onRequest(context) {
  //   const obj = await context.env.DOKSREF.get('ref');
  //   if (obj === null) {
  //     return new Response('Not found', { status: 404 });
  //   }
  //   return new Response(obj.body);
  // }

  export async function onRequest(context) {
    // Create a prepared statement with our query
    const ps = context.env.DOKSREF.prepare('SELECT * from ref');
    const data = await ps.first();
  
    return Response.json(data);
  }

